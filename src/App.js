
import './App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom';  
import CreateStudent from './Pages/createStudent';
import StudentList from './Pages/StudentList';
import EditStudent from "./Pages/editStudent";
import API from './Pages/api';
import Protected from "./Pages/Protected";

function App() {
  return (
    <div>
      <Router>
      <Route path='/createStudent' exact component={CreateStudent}/>
      <Route path='/' exact component={StudentList}/>
      <Route path='/editStudent' exact component={EditStudent}/>
      <Route path='/api' exact component={API}/>
      <Route path="/editStudent/:id">
      <Protected Cmp={EditStudent}/>
      </Route>
      </Router>
    </div>
  );
}

export default App;
