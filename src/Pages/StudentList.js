import React,{ useState, useEffect } from 'react';
import { makeGetRequest, makePostRequest } from "./utils";
import { Card, CardBody, CardHeader, Col, Row, Table } from 'reactstrap';
import { useHistory } from "react-router-dom";
import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/button';
import Typography from '@material-ui/core/Typography';

export default function StudentList(){
    
    const [student, setStudent] = useState([]);
    const [open2, setOpen2] = React.useState(false);
    const [deletestudid,setdeletestudid]=React.useState('');
    const history = useHistory();

   async function getallstudents()
  {
      var bodyFormData = new FormData();
      makeGetRequest("/getall/student", bodyFormData).then((response) =>{
          console.log(response);
      if(response.data.data !== undefined)
      {
        setStudent(response.data.data);
      }
      else
      {
        setStudent([]);
      }
      
    }).catch((err) => {            
             console.log("Invalid username password!!", err);
           });
    }
   

    async function deleteStudent() {	
    var bodyFormData = new FormData();	
    makePostRequest("/delete/student/"+ localStorage.getItem("deletestudid"), bodyFormData ).then((response) =>{	
      if (response.data.status === "1") {	
         alert("Record Deleted Sucesssfully");	
         localStorage.removeItem("deletestudid");	
         setOpen2(false);	
         history.push("/");
         getallstudents();
       }	
      else	
      {	
      alert("Error Failed to delete");	
      }	
      }).catch((err) => {	
         alert("There was an error!");	
         setOpen2(false);
         console.log("Invalid username password!!", err);	
       });	
      }

    const handleopendialog = (stud_id) =>{
    setOpen2(true);
    setdeletestudid(stud_id)
      localStorage.setItem('deletestudid',stud_id)
   };
  const handleClose2 = () =>
   {
      setOpen2(false);
      setTimeout(() => {
        }, 3000);
   };
   

  useEffect(() =>
   {
          getallstudents();
   }, []);
    return(
        <div>
        <div className="animated fadeIn">  
      <Row>  
        <Col>  
          <Card style={{display:'flex', flexDirection:'column', justifyContent:'center', textAlign:'center'}}>  
            <CardHeader>  
              <Typography variant="h4">Student Registration System</Typography>
              <Typography variant="h5">Student List</Typography> 
              <Button style={{margin:10}} variant="contained" color="primary" onClick={() =>history.push("/createStudent")}>Register Student</Button>
              </CardHeader>  
            <CardBody style={{display:'flex', justifyContent:'center'}}>  
              <Table style={{border: '1px solid gray'}} hover bordered striped responsive size="sm">  
                <thead>  
                  <tr style={{border: '1px solid gray', backgroundColor:'#DDDDDD'}}>  
                    <th style={{border: '1px solid gray', padding:10}}>id</th>                     
                    <th style={{border: '1px solid gray', padding:10}}>Name</th>  
                    <th style={{border: '1px solid gray', padding:10}}>Address</th>
                    <th style={{border: '1px solid gray', padding:10}}>Mobile No.</th>  
                    <th style={{border: '1px solid gray', padding:10}}>Edit/Delete</th>
                  </tr>  
                </thead>  
                <tbody>  
                  {  
                    student.map((item) => {  
                      return <tr >  
                        <td style={{border: '1px solid gray', backgroundColor:'#DDDDDD'}}>{item.stud_id}</td>  
                        <td style={{border: '1px solid gray'}}>{item.stud_name}</td>  
                        <td style={{border: '1px solid gray'}}>{item.stud_address}</td>
                        <td style={{border: '1px solid gray'}}>{item.stud_mono}</td>    
                        <td style={{border: '1px solid gray' , backgroundColor:'#DDDDDD'}}>  
                          <div className="btn-group">  
                            <Button style={{margin:10}} variant="contained"  color="primary"  onClick={() => history.push("/editStudent/"+item.stud_id)}>Edit</Button>  
                            <Button style={{margin:10}} variant="outlined" color="secondary"  onClick={() =>handleopendialog(item.stud_id)}>Delete</Button>  
                          </div>  
                        </td>  
                      </tr>  
                    })}  
                </tbody>  
              </Table>  
            </CardBody>  
          </Card>  
        </Col>  
      </Row>  
    </div>
          <Dialog open={open2} onClose={handleClose2}>
          <Typography style={{margin:20}} variant="h5">Do You Want to Delete This Student?</Typography>
          <div className="confirmButtns" style={{display:'flex', justifyContent:'space-evenly'}}>
                    <Button style={{margin:20}} variant="contained" color="primary" onClick={deleteStudent}>Yes</Button>
                    <Button style={{margin:20}} variant="contained" color="secondary" onClick={handleClose2}>No</Button>
          </div>
          </Dialog>
        </div>  
    );
}