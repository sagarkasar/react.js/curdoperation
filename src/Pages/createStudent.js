import React from 'react'  
import { makePostRequest } from "./utils";
import { useHistory } from "react-router-dom"; 
import { Card, CardBody, CardFooter, Col, Container, Form, Input, InputGroup, Row } from 'reactstrap';
import Button from '@material-ui/core/button';

export default function CreateStudent() {  
 
  const [studid, setStudid] = React.useState('');
  const [studname, setStudname] = React.useState('');
  const [studaddress, setStudaddress] = React.useState('');
  const [studmono, setStudmono] = React.useState(''); 
  const history = useHistory();
  
  
   async function insertstudentdata(e) {
     e.preventDefault();
     let bodyFormData = new FormData();
       
      bodyFormData.append("stud_id", studid);
      bodyFormData.append("stud_name", studname);
      bodyFormData.append("stud_address", studaddress);
      bodyFormData.append("stud_mono", studmono);
     
      makePostRequest("/student/reg", bodyFormData).then((response) => {
        if (response.data.status === "1") {
          console.log("Student Data Added")
          history.push("/");
        }
      })
        .catch((err) => {
          console.log("Invalid username password!!", err);
        });
      }

  const BacktoHome = () =>{
    history.push("/");
  }
  return (  
    <div className="app flex-row align-items-center">  
      <Container>  
        <Row className="justify-content-center">  
          <Col md="12" lg="10" xl="8">  
            <Card className="mx-4" style={{display:'flex',justifyContent:'center'}}>  
              <CardBody className="p-4">  
                <Form onSubmit={insertstudentdata}>  
                  <h1>Register</h1>  
                  <InputGroup className="mb-3">  
                    <Input type="text" name="ID" id="ID" placeholder="ID" value={studid} onChange={(e) =>(setStudid(e.target.value)) }  />  
                  </InputGroup>
                  <InputGroup className="mb-3">  
                    <Input type="text" name="Name" id="Name" placeholder="Name" value={studname} onChange={ (e) =>(setStudname(e.target.value)) }  />  
                  </InputGroup>  
                   <InputGroup className="mb-3">  
                    <Input type="text" placeholder="Address" name="Address" id="Address" value={studaddress} onChange={ (e) =>(setStudaddress(e.target.value)) }/>  
                  </InputGroup>  
                  <InputGroup className="mb-3">  
                    <Input type="text" placeholder="Mobile_no" name="Mobile_no" id="Mobile_no"  value={studmono} onChange={ (e) =>(setStudmono(e.target.value)) }  />  
                  </InputGroup>    
             <CardFooter className="p-4">  
                <Row style={{display:'flex', marginTop:10}}>  
                  <Col xs="12" sm="6">  
                    <Button style={{marginRight:20}} variant="contained" color="primary" type="submit" className="btn btn-info mb-1" ><span>Save</span></Button>  
                  </Col>  
                  <Col xs="12" sm="6">  
                    <Button variant="contained" color="Secondary" className="btn btn-info mb-1" onClick={() => BacktoHome()} ><span>Cancel</span></Button>  
                  </Col>  
                </Row>  
              </CardFooter>  
                </Form>  
              </CardBody>  
            </Card>  
          </Col>  
        </Row>  
      </Container>  
    </div>  
  )  
}  