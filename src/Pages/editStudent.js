import React, { useEffect } from 'react'  
import { useHistory, useParams } from "react-router-dom"; 
import { Card, CardBody, CardFooter, Col, Container, Form, Input, InputGroup,  Row } from 'reactstrap';  
import { makePostRequest, makeGetRequest } from "./utils";
import Button from '@material-ui/core/button';

 export default function EditStudent() { 
        const history = useHistory(); 
        const { id } = useParams();
        const [studname, setStudname] = React.useState('');
        const [studaddress, setStudaddress] = React.useState('');
        const [studmono, setStudmono] = React.useState(''); 

      async function viewstudent() {
    let bodyFormData = new FormData();
    makeGetRequest("/view/student/" + id, bodyFormData).then((response) => {
      setStudname(response.data.data[0].stud_name);
      setStudaddress(response.data.data[0].stud_address);
      setStudmono(response.data.data[0].stud_mono);

    }).catch((err) => {
      alert("There was an error!");
    });
  }

      async function UpdateStudents() {

      let bodyFormData = new FormData();
      bodyFormData.append("stud_name", studname);
      bodyFormData.append("stud_address", studaddress);
      bodyFormData.append("stud_mono", studmono);
      
      makePostRequest("/student/update/" + id, bodyFormData).then((response) => {
        if (response.data.status === "1") {
          alert('student Updated Successfully');
         history.push("/");
        }
        else {
          alert("failed to update student");
        }
      }).catch((err) => {
        alert("There was an error!");
      });
    }
        
        
  useEffect(() => {
   viewstudent();
  }, []);
  const BacktoHome = () =>{
    history.push("/");
  }

        return (  
                <div className="app flex-row align-items-center">  
                <Container>  
                <Row className="justify-content-center">  
                <Col md="12" lg="10" xl="8">  
                      <Card className="mx-4">  
                        <CardBody className="p-4" style={{display:'flex',justifyContent:'center'}}>  
                        <Form onSubmit={UpdateStudents}>  
                            <h1>Update Students</h1> 
                            
                  <InputGroup className="mb-3">  
                    <Input type="text" name="Name" id="Name" placeholder="Name" value={studname} onChange={ (e) =>(setStudname(e.target.value)) }  />  
                  </InputGroup>  
                   <InputGroup className="mb-3">  
                    <Input type="text" placeholder="Address" name="Address" id="Address" value={studaddress} onChange={ (e) =>(setStudaddress(e.target.value)) }/>  
                  </InputGroup>  
                  <InputGroup className="mb-3">  
                    <Input type="text" placeholder="Mobile_no" name="Mobile_no" id="Mobile_no"  value={studmono} onChange={ (e) =>(setStudmono(e.target.value)) }  />  
                  </InputGroup>    
                      <CardFooter className="p-4">  
                          <Row style={{display:'flex', marginTop:10}}>  
                            <Col xs="12" sm="6"> 
                              <Button style={{marginRight:20}} variant="contained" color="primary" type="submit"><span>Save</span></Button> 
                            </Col> 
                            <Col xs="12" sm="6"> 
                              <Button variant="contained" color="secondary" className="btn btn-info mb-1" onClick={() => BacktoHome()} ><span>Cancel</span></Button> 
                            </Col>
                          </Row> 
                      </CardFooter> 
                          </Form> 
                        </CardBody>                
                      </Card> 
                </Col> 
                </Row>  
                </Container>  
                </div>  
        )  
}  